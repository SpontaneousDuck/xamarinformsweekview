﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FormsWeekView
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        DateTime TodayDate = DateTime.Now.Date;
        Label lblWeekDate;
        Label lblTime;
        Frame frmColor;
        Button btnClick;
        string txtTimeAM = "AM";
        string txtTimePM = "PM";
        int iTime = 1;
        public MainPage()
        {
            InitializeComponent();

            CreateWeek();
        }

        private void CreateWeek()
        {
            //int iDay = (int)dateNow.Day;
            //int iMonth = (int)dateNow.Month;
            //int iYear = (int)dateNow.Year;
            //int iStartDayWeek = (int)new DateTime(iYear, iMonth, iDay).DayOfWeek;
            ////DateTime FirstDate = new DateTime(dateNow.Year, dateNow.Month, 1);
            ////int iDate = 1;
            //int daysInMonth = 0;
            //daysInMonth = DateTime.DaysInMonth(iYear, iMonth);
            ////DateTime EndDate = new DateTime(dateNow.Year, dateNow.Month, daysInMonth);

            //if (dateNow.Month == 2)
            //{
            //    if (DateTime.IsLeapYear(dateNow.Year))
            //    {
            //        daysInMonth = 29;
            //    }
            //}

            //int iDays = iDay;
            //if (iDay > iStartDayWeek)
            //{
            //    //Months = iMonth;
            //    iDays = iDay - iStartDayWeek;
            //}
            //else
            //{
            //    if (iDays < 8)    //if Date bet 1 to 6 and the Last Months dates
            //    {
            //        iDays = 1;

            //    }
            //}


            //Grid Week of Dates binding
            //for (int r = 2; r == 2; r++)
            //{
            //    for (int c = 3; c < grdDayOfWeek.ColumnDefinitions.Count; c++)
            //    {
            //        if (r == 2)
            //        {
            //            if (iDays <= daysInMonth && iDays < 32)
            //            {
            //                if (c % 2 != 0)
            //                {
            //                    iStartDayWeek = (int)new DateTime(iYear, iMonth, iDays).DayOfWeek;
            //                    if (c == iStartDayWeek * 2 + 3)
            //                    {
            //                        lblWeekDate = new Label() { Text = iDays.ToString(), TextColor = Color.Gray, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Start };
            //                        grdDayOfWeek.Children.Add(lblWeekDate, c, r);

            //                        if ((iStartDayWeek == 0 || iStartDayWeek == 1 || iStartDayWeek == 2 || iStartDayWeek == 3 || iStartDayWeek == 4 || iStartDayWeek == 5) || iDays != iStartDayWeek)
            //                            iDays++;
            //                        else
            //                            iDays--;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            grdWeekDates = new Grid();
            grdWeekDates.IsClippedToBounds = true;
            grdWeekDates.RowSpacing = 0;
            grdWeekDates.ColumnSpacing = 0;
            grdWeekDates.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Absolute) });

            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });

            for (int rowIndex = 1; rowIndex < 49; rowIndex++)
            {
                if (rowIndex % 2 == 0)
                {
                    grdWeekDates.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                }
                else
                {
                    grdWeekDates.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                }

                for (int columnIndex = 0; columnIndex < 17; columnIndex++)
                {
                    BoxView VerticalHoriBV;
                    if (columnIndex % 2 == 0)
                    {
                        VerticalHoriBV = new BoxView() { Color = Color.Gray, WidthRequest = 1, VerticalOptions = LayoutOptions.FillAndExpand };
                        grdWeekDates.Children.Add(VerticalHoriBV, columnIndex, rowIndex);
                        if (columnIndex == 0 && rowIndex % 2 == 0 && rowIndex != 48)
                        {
                            Grid.SetRowSpan(VerticalHoriBV, 2);
                        }
                    }

                    if (columnIndex % 2 != 0 && rowIndex % 2 == 0)
                    {
                        if (columnIndex == 1 && rowIndex % 2 == 0 && rowIndex != 48)
                        {
                            //Print Time
                            if (rowIndex <= 22)
                            {
                                lblTime = new Label() { Text = iTime.ToString() + " " + txtTimeAM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(0) };
                                grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                Grid.SetRowSpan(lblTime, 2);
                                iTime++;
                            }
                            if (rowIndex > 23 && rowIndex < 49)
                            {
                                if (iTime != 0)
                                {
                                    if (iTime == 12 && rowIndex == 25)
                                    {
                                        lblTime = new Label() { Text = iTime.ToString() + " " + txtTimePM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center };
                                        grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                        Grid.SetRowSpan(lblTime, 2);
                                    }
                                    else
                                    {
                                        lblTime = new Label() { Text = iTime.ToString() + " " + txtTimePM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(0) };
                                        grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                        Grid.SetRowSpan(lblTime, 2);
                                    }

                                    if (iTime == 12)
                                        iTime = 1;
                                    else
                                        iTime++;
                                }
                            }
                        }
                        else
                        {
                            VerticalHoriBV = new BoxView() { Color = Color.Gray, HorizontalOptions = LayoutOptions.FillAndExpand, HeightRequest = 1 };
                            grdWeekDates.Children.Add(VerticalHoriBV, columnIndex, rowIndex);
                        }
                    }

                    if (columnIndex % 2 != 0 && rowIndex % 2 != 0 && columnIndex != 1)
                    {
                        btnClick = new Button() { Text = "", BackgroundColor = Color.Transparent, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Margin = new Thickness(0) };
                        grdWeekDates.Children.Add(btnClick, columnIndex, rowIndex);
                    }

                }

            }
            ScrollWeek.Content = grdWeekDates;
            //grdUI.Children.Add(ScrollWeek, 0, 2);

        }

    }
}
