﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FormsWeekView.WeekView
{
    public class WeekView
    {
        ScrollView ParentView;
        public WeekView(ScrollView parentView)
        {
            ParentView = parentView;
        }

        private void CreateBackground()
        {
            int iTime = 1;
            string txtTimeAM = "AM";
            string txtTimePM = "PM";
            Label lblTime;

            var grdWeekDates = new Grid();
            grdWeekDates.IsClippedToBounds = true;
            grdWeekDates.RowSpacing = 0;
            grdWeekDates.ColumnSpacing = 0;
            grdWeekDates.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Absolute) });

            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Star });
            grdWeekDates.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) });

            for (int rowIndex = 1; rowIndex < 49; rowIndex++)
            {
                if (rowIndex % 2 == 0)
                {
                    grdWeekDates.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                }
                else
                {
                    grdWeekDates.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                }

                for (int columnIndex = 0; columnIndex < 17; columnIndex++)
                {
                    BoxView VerticalHoriBV;
                    if (columnIndex % 2 == 0)
                    {
                        VerticalHoriBV = new BoxView() { Color = Color.Gray, WidthRequest = 1, VerticalOptions = LayoutOptions.FillAndExpand };
                        grdWeekDates.Children.Add(VerticalHoriBV, columnIndex, rowIndex);
                        if (columnIndex == 0 && rowIndex % 2 == 0 && rowIndex != 48)
                        {
                            Grid.SetRowSpan(VerticalHoriBV, 2);
                        }
                    }

                    if (columnIndex % 2 != 0 && rowIndex % 2 == 0)
                    {
                        if (columnIndex == 1 && rowIndex % 2 == 0 && rowIndex != 48)
                        {
                            //Print Time
                            if (rowIndex <= 22)
                            {
                                lblTime = new Label() { Text = iTime.ToString() + " " + txtTimeAM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(0) };
                                grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                Grid.SetRowSpan(lblTime, 2);
                                iTime++;
                            }
                            if (rowIndex > 23 && rowIndex < 49)
                            {
                                if (iTime != 0)
                                {
                                    if (iTime == 12 && rowIndex == 25)
                                    {
                                        lblTime = new Label() { Text = iTime.ToString() + " " + txtTimePM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center };
                                        grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                        Grid.SetRowSpan(lblTime, 2);
                                    }
                                    else
                                    {
                                        lblTime = new Label() { Text = iTime.ToString() + " " + txtTimePM, TextColor = Color.Gray, FontSize = 15, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(0) };
                                        grdWeekDates.Children.Add(lblTime, columnIndex, rowIndex);
                                        Grid.SetRowSpan(lblTime, 2);
                                    }

                                    if (iTime == 12)
                                        iTime = 1;
                                    else
                                        iTime++;
                                }
                            }
                        }
                        else
                        {
                            VerticalHoriBV = new BoxView() { Color = Color.Gray, HorizontalOptions = LayoutOptions.FillAndExpand, HeightRequest = 1 };
                            grdWeekDates.Children.Add(VerticalHoriBV, columnIndex, rowIndex);
                        }
                    }

                    if (columnIndex % 2 != 0 && rowIndex % 2 != 0 && columnIndex != 1)
                    {
                        btnClick = new Button() { Text = "", BackgroundColor = Color.Transparent, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Margin = new Thickness(0) };
                        grdWeekDates.Children.Add(btnClick, columnIndex, rowIndex);
                    }

                }

            }
            ParentView.Content = grdWeekDates;
        }
    }
}
